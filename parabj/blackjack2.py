# Parallel Blackjack v2

import random
from numba import jit

# ----------------------------------------------------------------------------
# This method computes the value of a card (soft or hard)
# ----------------------------------------------------------------------------
def card_value(card, eval_type='hard'):
    if card in range(2,11):
        return card
    elif card in ['J','Q','K']:
        return 10
    elif card == 'A' and eval_type == 'soft':
        return 1
    elif card == 'A' and eval_type == 'hard':
        return 11
    else:
        raise ValueError('Invalid hand value or card type in hand')

# ----------------------------------------------------------------------------
# This method computes the value of a hand (soft or hard)
# ----------------------------------------------------------------------------
def hand_value(hand_list):

    # Always handle hard value cards first
    # -- sort aces to end of hand
    aces = []
    non_aces = []
    for card in hand_list:
        if card == 'A':
            aces.append(card)
        else:
            non_aces.append(card)
    hand_list = non_aces + aces

    value = 0
    for card in hand_list:
        if card in range(2,11):
            value = value + card
        elif card in ['J','Q','K']:
            value = value + 10
        elif card == 'A' and (value + 11) <= 21:
            # Use ace hard value if hand
            # wont exceed 21
            value = value + 11
        elif card == 'A':
            value = value + 1
        else:
            raise ValueError('Invalid hand value or card type in hand')
    return value

# ----------------------------------------------------------------------------
# This method returns the type of hand
# -- One of 'soft', 'pair', or ''
#    -> a soft hand contains at least 1 ace
#    -> a pair is a hand consisting of only two cards that are the same value
#    -> A hard hand
# ----------------------------------------------------------------------------
def hand_type(hand_list):

    if len(hand_list) < 2:
        raise ValueError('A player hand must have at least 2 cards!')

    if len(hand_list) == 2 and \
        (card_value(hand_list[0]) == card_value(hand_list[1])):
        return 'pair'

    if 'A' in hand_list:
        return 'soft'

    return ''

# ----------------------------------------------------------------------------
# This method determines if the supplied hand is blackjack
# ----------------------------------------------------------------------------
def is_blackjack(hand_list):
    if len(hand_list) == 2 and hand_value(hand_list) == 21:
        return True
    else:
        return False

# ----------------------------------------------------------------------------
# This method determines if the supplied hand has busted (value > 21)
# ----------------------------------------------------------------------------
def is_bust(hand_list):
    if hand_value(hand_list) > 21:
        return True
    else:
        return False

# ----------------------------------------------------------------------------
# This method creates a list containing integers and characters representing
# any number of suitless standard 52 card decks
# ----------------------------------------------------------------------------
def decks(number):
    suit = ['A', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K']
    deck = suit*4
    return deck*number


# ----------------------------------------------------------------------------
# This method compares the hands of a player and the dealer and awards
# a win and a loss, or a push
# ----------------------------------------------------------------------------
def winner(player, dealer):
    dealer_hand_value = hand_value(dealer.hand)
    player_hand_value = hand_value(player.hand)
    winner_flag = "push"
    # If the dealer has a blackjack and the player doesn't, the dealer wins
    if is_blackjack(dealer.hand) and not is_blackjack(player.hand):
        winner_flag = "dealer"
    # If the player has a blackjack and the dealer doesn't, the player wins
    elif is_blackjack(player.hand) and not is_blackjack(dealer.hand):
        winner_flag = "player"
    # If the player busts, the dealer wins regardless of his hand
    elif player_hand_value > 21:
        winner_flag = "dealer"
    # If the dealer bust, and we already confirmed that the player didn't, the player wins
    elif dealer_hand_value > 21:
        winner_flag = "player"
    # Finally, if none of the above conditions were met, the higher hand value wins
    elif dealer_hand_value > player_hand_value:
        winner_flag = "dealer"
    elif player_hand_value > dealer_hand_value:
        winner_flag = "player"
    # And if neither hand is higher, the result is a push
    return winner_flag

# ----------------------------------------------------------------------------
# This method compares the hands of a player and the dealer and awards
# a win and a loss, or a push to both. Hand statistics like busts and
# blackjacks are added for the player and dealer. The method returns
# True if the player (2nd arg) won and False if the dealer (1st arg) won.
# ----------------------------------------------------------------------------
def is_winner(dealer, player):

    dealer.stats.add_hand_played()
    player.stats.add_hand_played()

    if is_blackjack(player.hand):
        player.stats.add_blackjack()
    if is_blackjack(dealer.hand):
        dealer.stats.add_blackjack()
        
    player_bust = is_bust(player.hand)
    dealer_bust = is_bust(dealer.hand)

    if player_bust:
        player.stats.add_bust()
        player.stats.add_loss()
        dealer.stats.add_win()
        return False
    elif dealer_bust:
        player.stats.add_win()
        dealer.stats.add_loss()
        dealer.stats.add_bust()
        return True
    elif hand_value(dealer.hand) > \
          hand_value(player.hand):
        # Dealer won
        player.stats.add_loss()
        dealer.stats.add_win()
        return False
    elif hand_value(dealer.hand) < \
          hand_value(player.hand):
        # Player won
        player.stats.add_win()
        dealer.stats.add_loss()
        return True
    else:
        # Push
        player.stats.add_push()
        dealer.stats.add_push()

# ----------------------------------------------------------------------------
# This method creates a nested tuple truth-table representing a standard dealer
# Blackjack strategy (hit on 16 or lower, stand otherwise)
#
# -- The internal tuples represent the current player hand value,
#    the dealers up-card, the basic action to make, and if available the
#    advanced action to make

# -- Internal tuples:
#   (soft) (9, [ (6, 'S', 'D'), ... ])
#       -> (player_hand_value_soft [ (dealer_card, hit/stand, double/surrender), ... ])
#   (pair) ('K', [ (7, 'S', ''), ... ])
#       -> (pair_card (2-'A'), [ (dealer_card, hit/stand, double/split/surrender), ...])
#   (hard) (11, [ (7, 'H', 'D') ... ])
#       -> (player_hand_value, [ (dealer_card, hit/stand, double/surrender), ...])
#
#   A dealer card value of '-' indicates that the strategy is the same
#   regardless of the dealer card
# ----------------------------------------------------------------------------
def dealer_strategy(type=''):

    # This path assumes the player has a hand with an Ace
    if type == 'soft':

        # No strategy
        return ()

    # This path assumes the player has a 2-card hand with a pair
    elif type == 'pair':

        # No strategy
        return ()

    # This path is default strategy based on hard hand value
    else:
        return (
                # player hand value - 4-16
                (4, [ ('-', 'H', '') ]),
                (5, [ ('-', 'H', '') ]),
                (6, [ ('-', 'H', '') ]),
                (7, [ ('-', 'H', '') ]),
                (8, [ ('-', 'H', '') ]),                
                (9, [ ('-', 'H', '') ]),
                (10, [ ('-', 'H', '') ]),
                (11, [ ('-', 'H', '') ]),
                (12, [ ('-', 'H', '') ]),
                (13, [ ('-', 'H', '') ]),
                (14, [ ('-', 'H', '') ]),
                (15, [ ('-', 'H', '') ]),
                (16, [ ('-', 'H', '') ]),                  
                # player hand value - 17-21
                (17, [ ('-', 'S', '') ]),
                (18, [ ('-', 'S', '') ]),
                (19, [ ('-', 'S', '') ]),
                (20, [ ('-', 'S', '') ]),
                (21, [ ('-', 'S', '') ])
               )

# ----------------------------------------------------------------------------
# This method creates a nested tuple truth-table representing the basic
# Blackjack strategy
# @see https://en.wikipedia.org/wiki/Blackjack#Basic_strategy
#
# -- The internal tuples represent the current player hand value,
#    the dealers up-card, the basic action to make, and if available the
#    advanced action to make

# -- Internal tuples:
#   (soft) (9, [ (6, 'S', 'D'), ... ])
#       -> (player_hand_value_soft [ (dealer_card, hit/stand, double/surrender), ... ])
#   (pair) ('K', [ (7, 'S', ''), ... ])
#       -> (pair_card (2-'A'), [ (dealer_card, hit/stand, double/split/surrender), ...])
#   (hard) (11, [ (7, 'H', 'D') ... ])
#       -> (player_hand_value, [ (dealer_card, hit/stand, double/surrender), ...])
#
#   A dealer card value of '-' indicates that the strategy is the same
#   regardless of the dealer card
# ----------------------------------------------------------------------------
def basic_strategy(type=''):

    # This path assumes the player has a hand with an Ace
    if type == 'soft':

        # @todo
        return ()

    # This path assumes the player has a 2-card hand with a pair
    elif type == 'pair':

        # @todo
        return ()

    # This path is default strategy based on hard hand value
    else:
        return (
                # player hand value - 4-8
                (4, [ ('-', 'H', '') ]),
                (5, [ ('-', 'H', '') ]),
                (6, [ ('-', 'H', '') ]),
                (7, [ ('-', 'H', '') ]),
                (8, [ ('-', 'H', '') ]),                
                # player hand value - 9
                (9, [ (2, 'H', ''),
                      (3, 'H', 'D'),
                      (4, 'H', 'D'),
                      (5, 'H', 'D'),
                      (6, 'H', 'D'),
                      ('-', 'H', '')
                    ]
                ),
                # player hand value - 10
                (10, [ (2, 'H', 'D'),
                       (3, 'H', 'D'),
                       (4, 'H', 'D'),
                       (5, 'H', 'D'),
                       (6, 'H', 'D'),
                       (7, 'H', 'D'),
                       (8, 'H', 'D'),
                       (9, 'H', 'D'),
                       ('-', 'H', 'D')
                     ]
                ),
                # player hand value - 11
                (11, [ ('-', 'H', 'D') ]),
                # player hand value - 12
                (12, [ (2, 'H', ''),
                       (3, 'H', ''),
                       (4, 'S', ''),
                       (5, 'S', ''),
                       (6, 'S', ''),
                       ('-', 'H', '')
                     ]
                ),
                # player hand value - 13
                (13, [ (2, 'S', ''),
                       (3, 'S', ''),
                       (4, 'S', ''),
                       (5, 'S', ''),
                       (6, 'S', ''),
                       ('-', 'H', '')
                     ]
                ),             
                # player hand value - 14
                (14, [ (2, 'S', ''),
                       (3, 'S', ''),
                       (4, 'S', ''),
                       (5, 'S', ''),
                       (6, 'S', ''),
                       ('-', 'H', '')
                     ]
                ),     
                # player hand value - 15
                # @note - different if surrenders allowed
                (15, [ (2, 'S', ''),
                       (3, 'S', ''),
                       (4, 'S', ''),
                       (5, 'S', ''),
                       (6, 'S', ''),
                       ('-', 'H', '')
                     ]
                ),
                # player hand value - 16
                # @note - different if surrenders allowed
                (16, [ (2, 'S', ''),
                       (3, 'S', ''),
                       (4, 'S', ''),
                       (5, 'S', ''),
                       (6, 'S', ''),
                       ('-', 'H', '')
                     ]
                ),
                # player hand value - 17
                # @note - different if surrenders allowed
                (17, [ ('-', 'S', '') ]),
                # player hand value - 18-21
                (18, [ ('-', 'S', '') ]),
                (19, [ ('-', 'S', '') ]),
                (20, [ ('-', 'S', '') ]),
                (21, [ ('-', 'S', '') ])
               )


# ----------------------------------------------------------------------------
# @class Strategy
#
#   This class represents a Blackjack strategy
# ----------------------------------------------------------------------------
class Strategy:

    # Constructor --
    #   -- NOTE: adv_strats should be populated with double 'D', split 'S', etc
    #            if the strategy should use those options when available
    def __init__(self, strategy=dealer_strategy, advanced_strats=[]):
        self.strat_soft = strategy(type='soft')
        self.strat_pair = strategy(type='pair')
        self.strat_hard = strategy()
        self.adv_strats=advanced_strats
        
    # This method returns the player's next action based on their hand
    # and the dealer's up-card.
    # -- One of 'H', 'S', 'P', 'D', 'U', or ''
    #    -> (hit, stand, split, double, surrender, busted)
    def get_move(self, player_hand, dealer_card):

        ht = hand_type(player_hand)
        val = hand_value(player_hand)

        if val > 21:
            # Hand is busted
            return ''

        if not all(self.strat_hard): # empty tuple
            raise ValueError('Default hard strategy must not be empty')
        tt = self.strat_hard
        if(ht == 'soft' and not all(self.strat_soft)):
            tt = self.strat_soft
        if(ht == 'pair' and not all(self.strat_pair)):
            tt = self.strat_pair
            # Pair hand assessment is based on the value of a single
            # pair card -- since the other is the same value
            val = card_value(player_hand[0])

        # Evaluate the strategy
        next_move = ''
        for s in tt:
            if val == s[0]:
                for dcard in s[1]:
                    if dealer_card == dcard[0]:
                        if dcard[2] and dcard[2] in self.adv_strats:
                            next_move = dcard[2]
                            break
                        elif dcard[1]:
                            next_move = dcard[1]
                            break
                        else:
                            raise Exception('No option specified in strategy for dealer card'.format(dealer_card))
                    if dcard[0] == '-':
                        if dcard[2] and dcard[2] in self.adv_strats:
                            next_move = dcard[2]
                            break
                        elif dcard[1]:
                            next_move = dcard[1]
                            break
                        else:
                            raise Exception('No option specified in strategy for '.format(dealer_card))
                break
            else:
                continue
        
        if not next_move:
            raise Exception('No corresponding move found in strategy for hand: {}, dealer_card: {}'.format(player_hand, dealer_card))
                
        return next_move

# ----------------------------------------------------------------------------
# @class Table
#
#   This class represents a Blackjack table with a single dealer and a variable
#   number of players
# ----------------------------------------------------------------------------
class Table:

    def __init__(self, num_of_decks=4, debug=False):

        self.dealer = Player(dealer_strategy)
        self.players = []
        self.hands_played = 0
        self.debug = debug

        # The kitty contains all the cards that are not
        # currently in play or in the deck for the Table
        self.kitty = []
        self.deck = decks(4)
        # Shuffle the new deck
        self.shuffle()
        
    def add_players(self, num_of_players, strategy=basic_strategy()):
        for i in range(0, num_of_players):
            self.players.append(Player(strategy))
        
    @jit
    def run_hands_para(self, num_of_hands=1):
        for i in range(0,num_of_hands):
            self.run_hand()
        #print('Completed {} hands'.format(i))
    
    def run_hands(self, num_of_hands=1):
        for i in range(0,num_of_hands):
            self.run_hand()
        #print('Completed {} hands'.format(i))
    
    def run_hand(self):

        # NOTE: European-style blackjack -- no dealer hole card
        #       Player blackjack will push against a dealer blackjack

        self.deal()
        # Finish player hands
        for player in self.players:
            while(player.act(self)):
                pass

        # Finish dealer hand
        self.dealer.hand.append(self.draw_card()) # hole card
        while(self.dealer.act(self)):
            pass

        # Assess results
        for player in self.players:
            player_won = is_winner(self.dealer, player)
            if self.debug:
                if player_won:
                    print('Player{} won with {} over {}, hand {} over {}'.format(player.id, hand_value(player.hand), hand_value(self.dealer.hand), player.hand, self.dealer.hand))
                elif player_won == False:
                    print('Player{} lost with {} against {}, hand {} against {}'.format(player.id, hand_value(player.hand), hand_value(self.dealer.hand), player.hand, self.dealer.hand))
                else:
                    print('Player{} pushed with {} against {}, hand {} against {}'.format(player.id, hand_value(player.hand), hand_value(self.dealer.hand), player.hand, self.dealer.hand))

        # Discard the hands
        self.discard_hands()

    def deal(self):
    
        self.dealer.reset()
        self.dealer.hand.append(self.draw_card())

        for player in self.players:
            player.reset()
            player.hand.append(self.draw_card())
            player.hand.append(self.draw_card())

    # Requests a card from the top of the deck
    # -- Kitty is shuffled back into the deck
    #    if the shuffle card ('X') is drawn
    def draw_card(self):
        card = self.deck.pop(0)
        if card == 'X':
            self.shuffle()
            card = self.deck.pop(0)
        if card == 'X':
            raise ValueError('Card value cannot be shuffle card \'X\'')
        return card
    
    # This method adds the cards from all
    # players hands to the kitty
    def discard_hands(self):

        # Discard dealer hand
        self.kitty = self.kitty + self.dealer.hand
        self.dealer.reset()
    
        # Discard player hands
        for player in self.players:
            self.kitty = self.kitty + player.hand
            player.reset()
        
    # This method shuffles the deck if it or the kitty
    # does not contain a shuffle card (indicating that
    # a shuffle already happened)
    def shuffle(self):
        if self.deck.count('X') == 0 \
           and self.kitty.count('X') == 0:
            self.deck = self.deck + self.kitty
            # Shuffle 3 times?
            random.shuffle(self.deck)
            #random.shuffle(self.deck)
            #random.shuffle(self.deck)
            self.deck.append('X')

# ----------------------------------------------------------------------------
# @class Stats
#
#   This class represents a record of statistics for a player
# ----------------------------------------------------------------------------
class Stats:

    # Creates a Blackjack player's statistics
    def __init__(self):
        self.hands_played = 0
        self.wins = 0
        self.losses = 0
        self.pushes = 0
        self.busts = 0
        self.blackjacks = 0

    # This method adds 1 to the number of hands played
    def add_hand_played(self):
        self.hands_played = self.hands_played + 1

    # This method adds 1 to the number of wins
    def add_win(self):
        self.wins = self.wins + 1

    # This method adds 1 to the number of losses
    def add_loss(self):
        self.losses = self.losses + 1

    # This method adds 1 to the number of pushes
    def add_push(self):
        self.pushes = self.pushes + 1

    # This method adds 1 to the number of busts
    def add_bust(self):
        self.busts = self.busts + 1

    # This method adds 1 to the number of blackjacks
    def add_blackjack(self):
        self.blackjacks = self.blackjacks + 1

    def join(self, stats):
        self.hands_played = self.hands_played + stats.hands_played
        self.wins = self.wins + stats.wins
        self.losses = self.losses + stats.losses
        self.pushes = self.pushes + stats.pushes
        self.busts = self.busts + stats.busts
        self.blackjacks = self.blackjacks + stats.blackjacks
        
    def as_string(self):
        return 'hands_played: {}, wins: {}, losses: {}, pushes: {}, busts: {}, blackjacks: {}'.format( \
        self.hands_played, \
        self.wins, \
        self.losses, \
        self.pushes, \
        self.busts, \
        self.blackjacks)

# ----------------------------------------------------------------------------
# @class Player
#
#   This class represents a player or dealer in a game of Blackjack
# ----------------------------------------------------------------------------
class Player:

    gid = 0

    # Creates a Blackjack player
    def __init__(self, player_strategy=dealer_strategy, set_id=-1):

        # Every player created gets a unique id number
        if set_id == -1:
        
            self.id = Player.gid + 1
            Player.gid = Player.gid + 1
        else:
            self.id = set_id

        # Hand
        self.hand = []
        self.actions = []
        self.done = False

        # Stats
        self.stats = Stats()
        
        # Strategy
        self.strategy = Strategy(player_strategy)

    # Performs the next player action in a hand
    def act(self, table):
        if is_blackjack(self.hand):
            self.done = True
        move = self.strategy.get_move(self.hand, table.dealer.hand[0])
        # NOTE: The dealer will have two cards in their hand at this point
        #       The first is considered the up-card, the second the hole card
        if not move:
            self.actions.append(move)
        self.process_move(move, table)
        return not self.done

    def process_move(self, move, table):
        if move == 'H':
            self.hit(table)
        elif move == 'S' or not move:
            # Choosing to stand or hand is busted
            self.stand()
        # @todo Handle 'D', 'P', 'U'

    # Action - adds a card from the deck to the players hand
    def hit(self, table):
        self.hand.append(table.draw_card())

    # Action - indicates that the player has completed the hand
    def stand(self):
        self.done = True
        return self.done

    # Evaluates the current players hand if the hand is over
    def stat(self, dealer_hand_val):
        hand_val = hand_value(self.hand)
        if len(self.hand) == 2 and hand_val == 21:
            self.blackjacks = self.blackjacks + 1
        if hand_val > 21:
            self.busts = self.busts + 1

    # Returns the current value of the player's hand
    def value(self):
        return hand_value(self.hand)
    
    # Clear out any existing hand and deal 2 cards
    def deal_hand(self, deck):
        self.hand.clear()
        self.hand.append(deck.pop(0))
        self.hand.append(deck.pop(0))
        
    # Resets the player state for a new round
    def reset(self):
        self.hand.clear()
        self.actions.clear()
        self.done = False
