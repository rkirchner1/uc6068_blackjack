# Parallel Blackjack v1

import random

# hand_value() computes the value of a hand (soft or hard) 
def hand_value(hand_list, hand_type='soft'):
    hand_value = 0
    for card in hand_list:
        if card in range(2,11):
            hand_value = hand_value + card
        elif card[0] in ['J','Q','K']:
            hand_value = hand_value + 10
        elif card[0] == 'A' and hand_type == 'soft':
            hand_value = hand_value + 1
        elif card[0] == 'A' and hand_type == 'hard':
            hand_value = hand_value + 11
        else:
            return -1
    return hand_value

def decks(number):
    suit = ['A', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K']
    deck = suit*4
    return deck*number

class BJ_Player:

    def __init__(self, name='Bob'):
        self.hand = []
        self.wins = 0
        self.losses = 0
        self.pushes = 0
        self.blackjacks = 0
        self.busts = 0
        self.done = False
        self.standard_strategy = []
        self.soft_strategy = []
        self.hard_strategy = []
    
    def act(self, deck):
        self.blackjack()
        if not self.done:
            if hand_value(self.hand) < 17:
                self.hit(deck)
            else:
                self.stand()
            return True
        else:
            return False
            
    def strategy_act(self, deck):
        self.blackjack()
        self.strategy()
        while not self.done and hand_value(self.hand) < 22:
            if 'A' in self.hand:
                randVal = random.random()
                #TODO finish
                self.stand()
            else:
                randVal = random.random()
                hand_val = hand_value(self.hand)
                for strategy in self.standard_strategy:
                    if strategy[0] == hand_val:
                        if strategy[1] > randVal:
                            self.hit(deck)
                            continue
                        else:
                            self.stand()
                            continue
        
    def strategy(self):
        self.standard_strategy = ((4, 1.0), (5, 1.0), (6, 1.0), (7, 1.0), (8, 1.0), (9, 1.0), 
                                  (10, 1.0), (11, 1.0), (12, 1.0), (13, 1.0), (14, 1.0), (15, 0.5), 
                                  (16, 0.1), (17, 0.0), (18, 0.0), (19, 0.0), (20, 0.0), (21, 0.0))
        self.soft_strategy = ((4, 1.0), (5, 1.0), (6, 1.0), (7, 1.0), (8, 1.0), (9, 1.0), 
                              (10, 1.0), (11, 1.0), (12, 1.0), (13, 1.0), (14, 1.0), (15, 1.0), 
                              (16, 0.5), (17, 0.0), (18, 0.0), (19, 0.0), (20, 0.0), (21, 0.0))
        self.hard_strategy = ((4, 1.0), (5, 1.0), (6, 1.0), (7, 1.0), (8, 1.0), (9, 1.0), 
                              (10, 1.0), (11, 1.0), (12, 1.0), (13, 1.0), (14, 1.0), (15, 1.0), 
                              (16, 0.5), (17, 0.0), (18, 0.0), (19, 0.0), (20, 0.0), (21, 0.0))

    def blackjack(self):
        if len(self.hand) == 2 and hand_value(self.hand, hand_type='hard') == 21:
            self.blackjacks = self.blackjacks + 1
            self.done = True
            return True
        else:
            return False
        
    def deal(self, deck):
        self.done = False
        self.hand.clear()
        self.hand.append(deck.pop(0))
        self.hand.append(deck.pop(0))

    def hit(self, deck):
        self.hand.append(deck.pop(0))
        
    def stand(self):
        self.done = True
        return self.done

    def stat(self, dealer_hand_val):
        hand_val = hand_value(self.hand)
        if len(self.hand) == 2 and hand_val == 21:
            self.blackjacks = self.blackjacks + 1
        if hand_val > 21:
            self.busts = self.busts + 1
    
    def value(self):
        return hand_value(self.hand)