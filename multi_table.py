from multiprocessing import Lock, Process, Queue, current_process
from parabj.blackjack2 import *


def table_runner(number_of_rounds, outputs):
    t = Table(4)
    t.players.append(Player(basic_strategy))
    t.players.append(Player(basic_strategy))
    t.players.append(Player(basic_strategy))
    t.run_hands(number_of_rounds)

    outputs.put(t.players)
